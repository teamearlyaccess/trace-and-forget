# README #



### INSTALLING ###

[DOWNLOAD: Grab a copy of the latest build.](https://bitbucket.org/teamearlyaccess/trace-and-forget/downloads)

Copy the exe into the ININ Trace Initialization folder. The timer must be installed in the ININ Trace Initialization folder to run.

Common locations are:

* C:\I3\IC\ININ Trace Initialization
* C:\Program Files (x86)\Interactive Intelligence\ININ Trace Initialization

### What is this repository for? ###

* The trace config utility timer is an application to set trace levels and have them revert back to normal or custom settings after a set amount of time. The Trace Config Utility Timer was created during the 2015 Hackathon by Couch, Wes and Young, Evan.

### Who do I talk to? ###

* Email wes.couch@inin.com or evan.young@inin.com


### RECENT CHANGES ###
* Build 0.9.4 released
* Fixed issue with end levels not applying at the correct time
* Fixed issue with application not shutting down after end levels applied
* Fixed some logic issues with timers for application of levels. What was displayed in the confirmation box was correct but did not always match what was selected.

* Build 0.9.3 released
* New feature, return tracing to end levels when your drive is running out of space
* Set a space limit on your drive and the timer will revert to end levels when space limit is reached


* Build 0.9.2 released
* Includes a fix where default levels would be applied when the original option was selected.


:CURRENTVERSION:0.9.4: