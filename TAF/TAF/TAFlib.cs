﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using MessageBox = System.Windows.MessageBox;
using System.Reflection;

namespace TAF
{
    class TAFlib
    {
        Assembly asm;
        Type TopicMgr;
        MethodInfo get_topic_as_xml;
        MethodInfo set_topics_from_xml;
        public string CURRENTVERSION = "0.9.4";
        public string NewVersion = "";

        public string Get_topic_as_xml()
        {
            setupTopicMgr();

            object obj = Activator.CreateInstance(TopicMgr);
            //getting currentXml globally once

            return get_topic_as_xml.Invoke(obj, null).ToString();
        }

        public bool IsSpaceLimitReached(string driveLetter, long limit)
        {
            foreach (var drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady == false)
                {
                    continue;
                }
                if (drive.Name.ToString() == driveLetter)
                {
                    if (toGigabytesLong(drive.TotalFreeSpace) < limit)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void Set_topics_from_xml(string configXml)
        {
            setupTopicMgr();

            object obj = Activator.CreateInstance(TopicMgr);
            //getting currentXml globally once
            // Setup parameter
            object[] xml = { configXml };
            
            set_topics_from_xml.Invoke(obj, xml);
        }

        private static string loadDll()
        {
            // Add new versions at the top as they become available.
            string path = "";
            TAFlib find = new TAFlib();

            if (File.Exists("i3trace_dotnet_reader-w32r-5-0.dll"))
            {
                path = "i3trace_dotnet_reader-w32r-5-0.dll";
            }
            else if (File.Exists("i3trace_dotnet_reader-w32r-4-0.dll"))
            {
                path = "i3trace_dotnet_reader-w32r-4-0.dll";
            }
            else if (File.Exists("i3trace_dotnet_reader-w32r-2-1.dll"))
            {
                path = "i3trace_dotnet_reader-w32r-2-1.dll";
            }
            else if (File.Exists("i3trace_dotnet_reader-w32r-3-0.dll"))
            {
                path = "i3trace_dotnet_reader-w32r-3-0.dll";
            }
            else if (File.Exists(find.findNonSpecifiedDll()))
            {
                path = find.findNonSpecifiedDll();
            }
            else
            {
                MessageBox.Show("No trace configuration assemblies were found.\n\nIs this program installed in the ININ Trace Initialization folder?");
                MainWindow.shutdownApp();
                System.Environment.Exit(0);
            }
            return path;
        }

        public bool validateInteger(string num)
        {
            int result;
            if (Int32.TryParse(num, out result))
            {
                return true;
            }
            return false;
        }

        public string toGigabytesString(long bytes) // Converts bytes to Gigabytes for easy comparison
        {
            return (bytes / 1073741824).ToString();
        }

        public long toGigabytesLong(long bytes) // Converts bytes to Gigabytes for easy comparison
        {
            return (bytes / 1073741824);
        }

        private void setupTopicMgr()
        {
            asm = Assembly.LoadFrom(loadDll());
            TopicMgr = asm.GetType("TopicMgr");
            get_topic_as_xml = TopicMgr.GetMethod("get_topics_as_xml");
            set_topics_from_xml = TopicMgr.GetMethod("set_topics_from_xml");
        }

        public void launchProcess(string processFileName)
        {
            //processFileName is the full path of the file name. For example: @"C:\Program Files (x86)\Interactive Intelligence\ININ Trace Initialization\inintraceconfig-w32r-5-0.exe"
            try
            {
                if (!File.Exists(processFileName))
                {
                    //MessageBox.Show("File does not exist: " + processFileName);
                    return;
                }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                string workingDir = Assembly.GetEntryAssembly().Location;
                process.StartInfo.WorkingDirectory = workingDir.Substring(0, 3);
                //process.StartInfo.WorkingDirectory = "C:\\";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.FileName = processFileName;
                process.Start();
                process.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public string loadTraceExe()
        {
            // Add new versions at the top as they become available.
            string path = "";
            TAFlib find = new TAFlib();

            if (File.Exists("inintraceconfig-w32r-5-0.exe"))
            {
                path = "inintraceconfig-w32r-5-0.exe";
            }
            else if (File.Exists("inintraceconfig-w32r-4-0.exe"))
            {
                path = "inintraceconfig-w32r-4-0.exe";
            }
            else if (File.Exists("inintraceconfig-w32r-2-1.exe"))
            {
                path = "inintraceconfig-w32r-2-1.exe";
            }
            else if (File.Exists("inintraceconfig-w32r-3-0.exe"))
            {
                path = "inintraceconfig-w32r-3-0.exe";
            }
            else if (File.Exists(find.findNonSpecifiedExe()))
            {
                path = find.findNonSpecifiedExe();
            }
            else
            {
                MessageBox.Show("No trace configuration utility was found.\n\nIs this program installed in the ININ Trace Initialization folder?");
            }
            return path;
        }

        public string timeLeft(int minutes)
        {
            int tDays = 0;
            int tHours = 0;
            int tMinutes = minutes;
            string returnString = "";

            // Find total days
            tDays = (tMinutes / (24 * 60));

            // Find total hours
            if (tDays > 0)
            {
                tMinutes = tMinutes - (tDays * 24 * 60);
            }

            // Find total minutes
            tHours = (tMinutes / 60);


            // Return only minutes if no days or hours have been entered
            if (tHours > 0)
            {
                tMinutes = tMinutes - (tHours * 60);
            }

            if (tDays > 0)
            {
                returnString = tDays + " days, ";
            }
            if (tHours > 0)
            {
                returnString += tHours + " hours, and ";
            }

            returnString += tMinutes + " minutes";

            return returnString;
        }

        public string findNonSpecifiedDll()
        {

            string[] test = { "" };
            string final = "";
            try
            {
                test = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "i3trace_dotnet_reader-w32r-*");
                final = test[0];
            }
            catch (IndexOutOfRangeException e)
            {

            }
            return final;
        }

        public string findNonSpecifiedExe()
        {
            string[] test = { "" };
            string final = "";
            try
            {
                test = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "inintraceconfig*");
                final = test[0];
            }
            catch (IndexOutOfRangeException e)
            {
            }
            return final;
        }

        public bool isCurrentVersion()
        {
            string start = "<p>:CURRENTVERSION:";
            string end = ":</p>";
            string versionString = start + CURRENTVERSION + end;
            var url = "https://bitbucket.org/teamearlyaccess/trace-and-forget/overview";

            // This little section adds a dummy parameter on the end to get around WebClient caching, this needs to be implemented better.
            Random random = new Random();
            url += "?dummy=" + random.Next(0, 1000).ToString();
            var client = new WebClient();


            try
            {
                using (var stream = client.OpenRead(url))
                using (var reader = new StreamReader(stream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Length == versionString.Length)
                        {
                            if (line.ToString() == versionString)
                            {
                                return true;
                            }
                            string[] split = line.Split(':');
                            string[] splitVString = versionString.Split(':');
                            if (split[0] != splitVString[0])
                            {
                                continue;
                            }
                            if (split[1] == splitVString[1] && split[3] == splitVString[3])
                            {
                                NewVersion = split[2];
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception e)// If the website can not be contacted to check for an update display this message.
            {
                MessageBox.Show("The trace config utility was unable to check if an update is available.");
                return true;
            }
            return false;
        }
    }
}
