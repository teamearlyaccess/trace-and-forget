﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;

namespace TAF
{
    /// <summary>
    /// Interaction logic for DriveSelection.xaml
    /// </summary>
    public partial class DriveSelection : Window
    {
        #region // Public Variables
        public string DriveLetter = "";
        public long SpaceLimit;
        #endregion

        #region // Public Classes
        public class Drive // This allows MainWindow to access the variables once the form is filled out and complete.
        {
            public string Name { get; set; }
            public string SpaceLeft { get; set; }
        }
        #endregion

        #region // Public Methods
        public DriveSelection()
        {
            InitializeComponent();
            fillDriveDataGrid();
        }
        #endregion

        #region // Private Methods
        private void fillDriveDataGrid() // This gets information from the drives to fill the datagrid with space information
        {
            foreach (var drive in DriveInfo.GetDrives())
            {
                // if the drive isn't ready or doesn't appear to be a hard drive skip it
                if (drive.IsReady == false)
                {
                    continue;
                }

                // add the data from the drive to the datagrid
                TAFlib converter = new TAFlib();
                var data = new Drive { Name = drive.Name, SpaceLeft = converter.toGigabytesString(drive.TotalFreeSpace) + " GB" };
                dgDrives.Items.Add(data);
            }
        }

        private void setSpaceLimit() // Sets public variables so MainWindow can access them after the form is closed
        {
            long limit = long.Parse(txtSpaceLimit.Text);
            SpaceLimit = limit;
            var cellInfo = dgDrives.SelectedItem;
            string drive = ((Drive)(cellInfo)).Name;
            DriveLetter = drive;
        }

        private void saveForm() // Saves all the data and closes the form
        {
            // Check for good input
            if (isGoodInput() == true)
            {
                // Set variables
                setSpaceLimit();

                // Tell MainWindow the form was completed successfully
                this.DialogResult = true;
                this.Close();
            }
        }

        private bool isGoodInput() // Checks to make sure input is OK
        {
            int limit;
            if (dgDrives.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select a Drive.");
                return false;
            }

            if (txtSpaceLimit.Text == "")
            {
                MessageBox.Show("Please Enter a Space Limit.");
                return false;
            }

            if (!int.TryParse((txtSpaceLimit.Text), out limit))
            {
                MessageBox.Show("Please Enter a Space Limit, Example: 20");
                return false;
            }

            if (limit <= 0)
            {
                MessageBox.Show("Please Enter a Space Limit above 0.");
                return false;
            }

            int spaceLeft;
            string space = ((Drive)(dgDrives.SelectedItem)).SpaceLeft.ToString();
            space = space.Substring(0, space.Length - 3);
            int.TryParse(space, out spaceLeft);

            if (limit >= spaceLeft)
            {
                MessageBox.Show("The Space Limit entered is higher than the total space left on the " + ((Drive)(dgDrives.SelectedItem)).Name + " drive.");
                return false;
            }

            return true;
        }
        #endregion

        #region // Form Controls
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            saveForm();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            // Uncheck the space limit checkbox because the form was not completed
            MainWindow.UncheckSpaceLimit();
            this.Close();
        }

        private void txtSpaceLimit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                saveForm();
            }
        }
        #endregion
    }
}
