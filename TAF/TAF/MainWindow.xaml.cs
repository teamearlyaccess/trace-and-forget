﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MessageBox = System.Windows.MessageBox;
using Application = System.Windows.Application;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Drawing;
using Microsoft.Win32;
using System.Xml;
using System.Reflection;

namespace TAF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        #region // Setting up worker thread and public variables.

        TimerWorker timerWorkerObject = new TimerWorker();
        Thread timerThread;
        
        string defaultXml;

        const string TRACINGTYPE = "Start Config";
        const string REVERTTRACING = "End Config";
        
        public NotifyIcon systrayIcon = new NotifyIcon();
        public ContextMenu sysMenu = new ContextMenu();

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            //stuff
            setupXml();

            initializeSystray();
            initializeContextMenu();
            checkVersion();
        }

        public static void shutdownApp()
        {
            Application.Current.Shutdown();
        }

        #region // Private Functions

        private void checkVersion()
        {
            TAFlib versionChecker = new TAFlib();
            MessageBoxResult result = MessageBoxResult.No;
            if (versionChecker.isCurrentVersion() == false)
            {
                result = MessageBox.Show("There is a new version available. Do you want to download it?\n\nCurrent Version: "
                        + versionChecker.CURRENTVERSION
                        + "\nNew Version: "
                        + versionChecker.NewVersion, "", MessageBoxButton.YesNo);
            }

            if (result == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("https://bitbucket.org/teamearlyaccess/trace-and-forget/overview");
            }
        }

        private void setupXml()
        {
            TAFlib xmlTool = new TAFlib();
            timerWorkerObject.currentXml = xmlTool.Get_topic_as_xml();
            //timerWorkerObject.currentXml = get_topic_as_xml.Invoke(obj, null).ToString();
            //gettign defaultXml globally once. Requires currentXml to be set
            defaultXml = getDefaultTrace();
        }

        private string getDefaultTrace()
        {
            XmlDocument tempXmlTraceDoc = new XmlDocument();
            tempXmlTraceDoc.LoadXml(timerWorkerObject.currentXml);
            foreach (XmlNode node in tempXmlTraceDoc.SelectNodes("/ININTraceTopics/Topic"))
            {
                node.Attributes["level"].Value = node.Attributes["initialLevel"].Value;
            }
            return tempXmlTraceDoc.OuterXml;
        }

        private bool traceTopicExists(string path)
        {
            if (path == "")
            {
                MessageBox.Show("Please enter a " + TRACINGTYPE + " configuration file.");
                return false;
            }
            else if (!File.Exists(path))
            {
                MessageBox.Show("Invalid " + TRACINGTYPE + " configuration file chosen.");
                return false;
            }
            else if (File.Exists(path))
            {
                return true;
            }
            return false;
        }

        private bool traceRevertTopicExists(string path)
        {
            if (path == "")
            {
                MessageBox.Show("Please enter a " + REVERTTRACING + " configuration file.");
                return false;
            }
            else if (!File.Exists(path))
            {
                MessageBox.Show("Invalid " + REVERTTRACING + " configuration file chosen.");
                return false;
            }
            else if (File.Exists(path))
            {
                return true;
            }
            return false;
        }

        private bool confirmTime()
        {
            string message = "";
            if (rdbCustomTracing.IsChecked == true)
            {
                message = "The Custom trace configuration will be applied ";
            }
            else if (rdbDefaultTracing.IsChecked == true)
            {
                message = "The Default trace levels will be applied ";
            }
            else
            {
                message = "The Original levels will stay the same ";
            }

            if (rdbNow.IsChecked == true || rdbCurrentTracing.IsChecked == true)
            {
                message += "on start.";
            }
            else
            {
                message += "after ";
                message += "\n" + totalApplyTime() + ".";
            }



            if (rdbRevertCurrentTracing.IsChecked == true)
            {
                message += "\n\nThe trace levels will End with the Original trace levels after ";
            }
            else if (rdbRevertCustomTracing.IsChecked == true)
            {
                message += "\n\nThe trace levels will End with the Custom trace configuration levels after ";
            }
            else if (rdbRevertDefaultTracing.IsChecked == true)
            {
                message += "\n\nThe trace levels will End with the Default trace levels after ";
            }
            message += "\n" + totalRevertTime() + ".";

            // Append extra text if same custom file is selected
            // If both custom fields are blank, this will still append the extra text. 
            // traceTopicExists checks that the boxes aren't blank and this is ran in setTraceXml which is called before confirmTime is called for btnApply. This might be an edge case (both custom trace files are blank) if only confirmTime is called in the future.
            // Could add traceTopicExist here but then btnApply would run it twice if both custom radio buttons are checked
            // Add string note and set inside a nested check above? This will reduce the number of checks from ui and the code but potentially could make it harder to read.
            if(rdbCustomTracing.IsChecked == true && rdbRevertCustomTracing.IsChecked == true)
            {
                if (txtPath.Text == txtRevertPath.Text)
                {
                    message += "\n\n\nNOTE: The custom files you have selected are the same. Is this correct?";
                }
            }

            var confirmResult = MessageBox.Show(message, "Confirmation", MessageBoxButton.OKCancel);

            if (confirmResult == MessageBoxResult.OK)
            {
                return true;
            }
            else
            {
                timerWorkerObject.TotalApplyMinutes = 0;
                timerWorkerObject.TotalRevertMinutes = 0;
                return false;
            }
        }

        private void initializeSystray()
        {
            //Set an icon
            systrayIcon.Icon = TAF.Properties.Resources.favicon64x;

            //Setup mouse over handler
            systrayIcon.MouseMove += new MouseEventHandler(systrayIcon_MouseMove);
            systrayIcon.ContextMenu = sysMenu;
        }

        private void initializeContextMenu()
        {
            MenuItem applyExit = new MenuItem();
            MenuItem addTime = new MenuItem();
            MenuItem subtractTime = new MenuItem();

            //applyExit sub MenuItems
            MenuItem applyTrace = new MenuItem();
            MenuItem applyRevert = new MenuItem();
            MenuItem applyPrevious = new MenuItem();

            #region // Unused Events for add and subtract time
            /*
            addTime.Click += new EventHandler(addTime_Click);
            subtractTime.Click += new EventHandler(subtractTime_Click);
             */
            #endregion

            //applyExit sub MenuHandlers
            applyTrace.Click += new EventHandler(applyTrace_Click);
            applyRevert.Click += new EventHandler(applyRevert_Click);
            applyPrevious.Click += new EventHandler(applyPrevious_Click);

            applyExit.Text = "Apply and Exit";
            addTime.Text = "Add More Time";
            subtractTime.Text = "Subtract Time";
            applyTrace.Text = "Apply Start Levels and Exit";
            applyRevert.Text = "Apply End Levels and Exit";
            applyPrevious.Text = "Apply Original Levels and Exit";

            //add items to applyExit submenu
            applyExit.MenuItems.Add(applyPrevious);
            applyExit.MenuItems.Add(applyTrace);
            applyExit.MenuItems.Add(applyRevert);
            

            //add Items to sysMenu
            sysMenu.MenuItems.Add(addTime);
            sysMenu.MenuItems.Add(subtractTime);
            sysMenu.MenuItems.Add("-");
            sysMenu.MenuItems.Add(applyExit);
            
        }

        private string totalApplyTime()
        {
            // Initialize variables
            int tMinutes = new int();

            if (gridTime.Visibility == Visibility.Visible)
            {
                if (rdbFiveMinutes.IsChecked == true)
                {
                    tMinutes = 5;
                }
                else if (rdbTenMinutes.IsChecked == true)
                {
                    tMinutes = 10;
                }
                else if (rdbThirtyMinutes.IsChecked == true)
                {
                    tMinutes = 30;
                }
                else if (rdbOneHour.IsChecked == true)
                {
                    tMinutes = 60;
                }
                else if (rdbFiveHours.IsChecked == true)
                {
                    tMinutes = 300;
                }
                else if (rdbCustomTime.IsChecked == true)
                {
                    int minutes = int.Parse(txtMinutes.Text);
                    int hours = int.Parse(txtHours.Text);
                    int days = int.Parse(txtDays.Text);

                    tMinutes = minutes + (hours * 60) + (days * 24 * 60);
                }
            }
            else
            {
                tMinutes = 0;
            }
            
            // Sets total minutes left on timer.
            timerWorkerObject.TotalApplyMinutes = tMinutes;
            TAFlib timecalc = new TAFlib();
            return timecalc.timeLeft(timerWorkerObject.TotalApplyMinutes);
        }

        private string totalRevertTime()
        {
            // Initialize variables
            int tMinutes = new int();

            if (gridTime.Visibility == Visibility.Visible)
            {
                if (rdbRevertFiveMinutes.IsChecked == true)
                {
                    tMinutes = 5;
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
                else if (rdbRevertTenMinutes.IsChecked == true)
                {
                    tMinutes = 10;
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
                else if (rdbRevertThirtyMinutes.IsChecked == true)
                {
                    tMinutes = 30;
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
                else if (rdbRevertOneHour.IsChecked == true)
                {
                    tMinutes = 60;
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
                else if (rdbRevertFiveHours.IsChecked == true)
                {
                    tMinutes = 300;
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
                else if (rdbRevertCustomTime.IsChecked == true)
                {
                    int minutes = int.Parse(txtRevertMinutes.Text);
                    int hours = int.Parse(txtRevertHours.Text);
                    int days = int.Parse(txtRevertDays.Text);

                    tMinutes = minutes + (hours * 60) + (days * 24 * 60);
                    tMinutes += timerWorkerObject.TotalApplyMinutes;
                }
            }
            else
            {
                if (rdbRevertFiveMinutes.IsChecked == true)
                {
                    tMinutes = 5;
                }
                else if (rdbRevertTenMinutes.IsChecked == true)
                {
                    tMinutes = 10;
                }
                else if (rdbRevertThirtyMinutes.IsChecked == true)
                {
                    tMinutes = 30;
                }
                else if (rdbRevertOneHour.IsChecked == true)
                {
                    tMinutes = 60;
                }
                else if (rdbRevertFiveHours.IsChecked == true)
                {
                    tMinutes = 300;
                }
                else if (rdbRevertCustomTime.IsChecked == true)
                {
                    int minutes = int.Parse(txtRevertMinutes.Text);
                    int hours = int.Parse(txtRevertHours.Text);
                    int days = int.Parse(txtRevertDays.Text);

                    tMinutes = minutes + (hours * 60) + (days * 24 * 60);
                }
            }
            // Sets total minutes left on timer.
            timerWorkerObject.TotalRevertMinutes = tMinutes;
            TAFlib timecalc = new TAFlib();
            return timecalc.timeLeft(timerWorkerObject.TotalRevertMinutes);
        }

        private void minimizeToSystray()
        {
            updateSystray(timerWorkerObject);

            //systrayIcon.BalloonTipText = "Test Balloon Tip.";
            systrayIcon.Visible = true;
            //systrayIcon.ShowBalloonTip(500);
            this.Hide();
        }

        private void updateSystray(TimerWorker timer)
        {
            timerWorkerObject.updateTimeLeft(timerWorkerObject);

            if (!timer.applyComplete)
            {
                systrayIcon.Text = timer.TimeLeftApply;
            }
            else if (!timer.revertComplete)
            {
                systrayIcon.Text = timer.TimeLeftRevert;
            }
            else
            {
                systrayIcon.Text = "End levels applied.";
                Thread.Sleep(100);
                Application.Current.Shutdown();
            }
        }

        private void shutdownProgram(Thread t)
        {
            timerThread.Abort();
            t.Abort();
            Application.Current.Shutdown();
        }

        private void previousExit()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit? The timer will be closed and tracing will be changed to the Original level settings.", "Confirmation", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                bool test;
                ApplyWorker worker = new ApplyWorker();
                worker.configXml = timerWorkerObject.currentXml;
                Thread exitThread = new Thread(worker.ApplySettings);
                exitThread.Start();
                shutdownProgram(exitThread);
            }
        }

        private void revertExit()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit? The timer will be closed and tracing will be changed to the End level settings.", "Confirmation", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                ApplyWorker worker = new ApplyWorker();
                worker.configXml = timerWorkerObject.userSelectedRevertXml;
                Thread exitThread = new Thread(worker.ApplySettings);
                exitThread.Start();
                shutdownProgram(exitThread);
            }
        }

        private void traceExit()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit? The timer will be closed and tracing will be changed to the Start level settings.", "Confirmation", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                ApplyWorker worker = new ApplyWorker();
                worker.configXml = timerWorkerObject.userSelectedTraceXml;
                Thread exitThread = new Thread(worker.ApplySettings);
                exitThread.Start();
                shutdownProgram(exitThread);
            }
        }

        private string browseFile()
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Filter = "ININ Topic Files (*.inintopic)|*.inintopic|All files (*.*)|*.*";
            fileDlg.Title = "Select a Topic File to Apply After a Set Amount of Time";

            if (fileDlg.ShowDialog() == true)
            {
                return fileDlg.FileName;
            }
            else
            {
                return "";
            }
        }

        private bool validateCustomTime()
        {
            TAFlib validater = new TAFlib();

            if (!validater.validateInteger(txtMinutes.Text))
            {
                MessageBox.Show("Invalid " + TRACINGTYPE + " minute entered");
                txtMinutes.Focus();
                return false;
            }
            if (!validater.validateInteger(txtHours.Text))
            {
                MessageBox.Show("Invalid " + TRACINGTYPE + " hours entered");
                txtHours.Focus();
                return false;
            }
            if (!validater.validateInteger(txtDays.Text))
            {
                MessageBox.Show("Invalid " + TRACINGTYPE + " days entered");
                txtDays.Focus();
                return false;
            }
            if (!validater.validateInteger(txtRevertMinutes.Text))
            {
                MessageBox.Show("Invalid " + REVERTTRACING + " minute applied");
                txtRevertMinutes.Focus();
                return false;
            }
            if (!validater.validateInteger(txtRevertHours.Text))
            {
                MessageBox.Show("Invalid " + REVERTTRACING + " hours applied");
                txtRevertHours.Focus();
                return false;
            }
            if (!validater.validateInteger(txtRevertDays.Text))
            {
                MessageBox.Show("Invalid " + REVERTTRACING + " days applied");
                txtRevertDays.Focus();
                return false;
            }
            return true;
        }

        private bool setTraceXml()
        {
            if (rdbCustomTracing.IsChecked == true)
            {
                if (traceTopicExists(txtPath.Text) == false)
                {
                    return false;
                }
                XmlDocument customTraceDoc = new XmlDocument();
                customTraceDoc.Load(txtPath.Text);
                timerWorkerObject.userSelectedTraceXml = customTraceDoc.InnerXml;
            }
            else if (rdbDefaultTracing.IsChecked == true)
            {
                timerWorkerObject.userSelectedTraceXml = defaultXml;
            }
            else if (rdbCurrentTracing.IsChecked == true)
            {
                timerWorkerObject.userSelectedTraceXml = timerWorkerObject.currentXml;
            }

            if (rdbRevertCustomTracing.IsChecked == true)
            {
                if (traceRevertTopicExists(txtRevertPath.Text) == false)
                {
                    return false;
                }
                XmlDocument customRevertTraceDoc = new XmlDocument();
                customRevertTraceDoc.Load(txtRevertPath.Text);
                timerWorkerObject.userSelectedRevertXml = customRevertTraceDoc.InnerXml;
            }
            else if (rdbRevertDefaultTracing.IsChecked == true)
            {
                timerWorkerObject.userSelectedRevertXml = defaultXml;
            }
            else if (rdbRevertCurrentTracing.IsChecked == true)
            {
                timerWorkerObject.userSelectedRevertXml = timerWorkerObject.currentXml;
            }
            return true;
        }

        private void applyTraceNow()
        {
            ApplyWorker worker = new ApplyWorker();
            worker.configXml = timerWorkerObject.userSelectedTraceXml;
            Thread exitThread = new Thread(worker.ApplySettings);
            timerWorkerObject.applyComplete = true;
        }

        private bool isSameCheck()
        {
            if ((rdbCurrentTracing.IsChecked == true && rdbRevertCurrentTracing.IsChecked == true)
                || (rdbDefaultTracing.IsChecked == true && rdbRevertDefaultTracing.IsChecked == true))
            {
                MessageBox.Show("Current settings will have no effect when ran, please change Start or End tracing type.");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void startWorkerThread()
        {
            timerThread = new Thread(timerWorkerObject.IncrementTimer);
            timerThread.Start();
        }
        #endregion

        #region // Button Controls
        private void btnTraceConfig_Click(object sender, RoutedEventArgs e)
        {
            TAFlib launcher = new TAFlib();
            launcher.launchProcess(launcher.loadTraceExe());
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            txtPath.Text = browseFile();
        }

        private void btnRevertBrowse_Click(object sender, RoutedEventArgs e)
        {
            txtRevertPath.Text = browseFile();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            if (!setTraceXml())
            {
                return;
            }

            if (!validateCustomTime())
            {
                return;
            }
            if (isSameCheck() == false)
            {
                return;
            }

            if (confirmTime() == false)
            {
                return;
            }

            if (rdbNow.IsChecked == true || rdbCurrentTracing.IsChecked == true)
            {
                timerWorkerObject.applyComplete = true;
                ApplyWorker worker = new ApplyWorker();
                if (rdbCurrentTracing.IsChecked == true)
                {
                    worker.configXml = timerWorkerObject.currentXml;
                }
                else
                {
                    worker.configXml = timerWorkerObject.userSelectedTraceXml;
                }
                Thread exitThread = new Thread(worker.ApplySettings);
                exitThread.Start();
            }

            minimizeToSystray();
            startWorkerThread();
        }
        #endregion

        #region // Radio Button Controls

        private void rdbCustomTracing_Checked(object sender, RoutedEventArgs e)
        {
            canCustomTracing.IsEnabled = true;
        }

        private void rdbCustomTracing_Unchecked(object sender, RoutedEventArgs e)
        {
            canCustomTracing.IsEnabled = false;
        }

        private void rdbCustomTime_Checked(object sender, RoutedEventArgs e)
        {
            gridCustomTime.IsEnabled = true;
        }

        private void rdbCustomTime_Unchecked(object sender, RoutedEventArgs e)
        {
            gridCustomTime.IsEnabled = false;
        }

        private void rdbRevertCustomTime_Checked(object sender, RoutedEventArgs e)
        {
            gridRevertCustomTime.IsEnabled = true;
        }

        private void rdbCurrentTracing_Checked(object sender, RoutedEventArgs e)
        {
            if (gridTime != null)
            {
                gridTime.IsEnabled = false;
                gridTime.Visibility = Visibility.Hidden;
                lblApplyTrace.Visibility = Visibility.Visible;
            }
        }

        private void rdbCurrentTracing_Unchecked(object sender, RoutedEventArgs e)
        {
            gridTime.IsEnabled = true;
            gridTime.Visibility = Visibility.Visible;
            lblApplyTrace.Visibility = Visibility.Hidden;
        }

        private void rdbRevertCustomTime_Unchecked(object sender, RoutedEventArgs e)
        {
            gridRevertCustomTime.IsEnabled = false;
        }

        private void rdbRevertCustomTracing_Unchecked(object sender, RoutedEventArgs e)
        {
            canRevertCustomTracing.IsEnabled = false;
        }

        private void rdbRevertCustomTracing_Checked(object sender, RoutedEventArgs e)
        {
            canRevertCustomTracing.IsEnabled = true;
        }

        #endregion

        #region // Text Box Controls
        private void txtMinutes_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtMinutes.SelectAll();
        }
        private void txtMinutes_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtMinutes.SelectAll();
        }
        private void txtHours_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtHours.SelectAll();
        }
        private void txtHours_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtHours.SelectAll();
        }
        private void txtDays_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtDays.SelectAll();
        }
        private void txtDays_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtDays.SelectAll();
        }
        private void txtRevertMinutes_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtRevertMinutes.SelectAll();
        }
        private void txtRevertMinutes_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtRevertMinutes.SelectAll();
        }
        private void txtRevertHours_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtRevertHours.SelectAll();
        }
        private void txtRevertHours_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtRevertHours.SelectAll();
        }
        private void txtRevertDays_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txtRevertDays.SelectAll();
        }
        private void txtRevertDays_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            txtRevertDays.SelectAll();
        }
        #endregion

        #region // SystrayIcon Controls
        private void applyTrace_Click(object sender, EventArgs e)
        {
            traceExit();
        }

        private void applyRevert_Click(object sender, EventArgs e)
        {
            revertExit();
        }

        private void applyPrevious_Click(object sender, EventArgs e)
        {
            previousExit();
        }

        /* Unused events for adding time, will add later
        private void addTime_Click(object sender, EventArgs e)
        {

        }

        private void subtractTime_Click(object sender, EventArgs e)
        {

        }
        */

        private void systrayIcon_MouseMove(object sender, MouseEventArgs e)
        {
            updateSystray(timerWorkerObject);
        }

        #endregion


        private void checkSpaceLimit_Checked(object sender, RoutedEventArgs e)
        {
            DriveSelection popup = new DriveSelection();
            if (popup.ShowDialog().Value == true)
            {
                lblLimit.IsEnabled = true;
                lblDrive.IsEnabled = true;

                timerWorkerObject.spaceLimit = popup.SpaceLimit;
                timerWorkerObject.drive = popup.DriveLetter;
                timerWorkerObject.driveCheck = true;

                lblDriveOutput.Content = popup.DriveLetter;
                lblLimitOutput.Content = popup.SpaceLimit.ToString() + " GB";

                popup.Close();
            }
            else
            {
                checkSpaceLimit.IsChecked = false;
            }
        }

        private void checkSpaceLimit_Unchecked(object sender, RoutedEventArgs e)
        {
            lblLimit.IsEnabled = false;
            lblDrive.IsEnabled = false;

            lblDriveOutput.Content = "";
            lblLimitOutput.Content = "";
        }

        public static void UncheckSpaceLimit()
        {
            MainWindow foo = new MainWindow();
            foo.uncheckSpaceLimit();
            foo.Close();
        }

        private void uncheckSpaceLimit()
        {
            checkSpaceLimit.IsChecked = false;
        }
    }
}