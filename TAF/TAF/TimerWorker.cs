﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TAF
{
    class TimerWorker
    {
        #region // Public Variables and Constants
        const int HOWLONGISAMINUTE = 36000;

        public string TimeLeftApply = "";
        public string TimeLeftRevert = "";

        public int TotalApplyMinutes = 0;
        public int TotalRevertMinutes = 0;

        public bool revertComplete = false;
        public bool applyComplete = false;

        public string userSelectedTraceXml = "";
        public string userSelectedRevertXml = "";
        public string currentXml = "";

        public string drive = "";
        public long spaceLimit = 0;
        public bool driveCheck = false;
        #endregion
        #region // Initialization of worker threads
        // Start worker to set initial settings when timer complete
        ApplyWorker applyWorkerObject = new ApplyWorker();
        Thread applyThread;

        // Start worker to set revert settings when last timer is complete
        ApplyWorker revertWorkerObject = new ApplyWorker();
        Thread revertThread;

        #endregion

        private volatile bool shouldStop;
        
        public void IncrementTimer()
        {
            setupWorkers();
            while (!shouldStop)
            {
                Thread.Sleep(HOWLONGISAMINUTE); // Wait one minute between Increments

                TotalApplyMinutes += -1;
                TotalRevertMinutes += -1;

                if (TotalApplyMinutes <= 0 && applyComplete == false)
                {
                    applyTimerComplete();
                }

                if (TotalRevertMinutes <= 0 && revertComplete == false)
                {
                    revertTimerComplete();
                    shouldStop = true;
                }

                if (driveCheck == true)
                {
                    if (spaceLimitReached() == true && revertComplete == false)
                    {
                        ApplyWorker worker = new ApplyWorker();
                        worker.configXml = userSelectedRevertXml;
                        TAFlib xmlTool = new TAFlib();
                        xmlTool.Set_topics_from_xml(userSelectedRevertXml);
                        Environment.Exit(0);
                    }
                }
                // WE NEED A CHECK HERE SO WHEN A TIMER IS DONE IT EXECUTES MAINWINDOW.APPLY or MAINWINDOW.REVERT
                // do we tho^?
                // Updates the time left 
                updateTimeLeft(this);
            }
        }

        private void setupWorkers()
        {
            if (applyComplete == false)
            {
                applyWorkerObject.configXml = userSelectedTraceXml;
                applyWorkerObject.currentXml = currentXml;
            }
            revertWorkerObject.configXml = userSelectedRevertXml;
            revertWorkerObject.currentXml = currentXml;
        }

        private void applyTimerComplete()
        {
            if (applyComplete == false)
            {
                applyComplete = true;
                applyThread = new Thread(applyWorkerObject.ApplySettings);
                applyThread.Start();
            }
        }

        private void revertTimerComplete()
        {
            revertComplete = true;
            revertThread = new Thread(revertWorkerObject.ApplySettings);
            revertThread.Start();
        }

        public void updateTimeLeft(TimerWorker timer)
        {
            TAFlib timecalc = new TAFlib();
            timer.TimeLeftApply = "Time Till Start: " + timecalc.timeLeft(timer.TotalApplyMinutes);
            timer.TimeLeftRevert = "Time Till End: " + timecalc.timeLeft(timer.TotalRevertMinutes);
        }

        public void RequestStop()
        {
            shouldStop = true;
        }

        private bool spaceLimitReached()
        {
            TAFlib spaceChecker = new TAFlib();
            return spaceChecker.IsSpaceLimitReached(drive, spaceLimit);
        }
    }
}
